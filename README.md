# LIS4368 - Advanced Web Applications Development

## Vincent Sibley

### P2 Requirements:

>1. Provide Bitbucket read-only access to p2 repo, *must* include README.md, using Markdown
syntax.
>
>2. Blackboard Links: p2 Bitbucket repo




#### Assignment Screenshots:

*Customer*:
![Customer](img/intro.png)
>
*Passed validation*:
![Passed validation](img/thanks.png)
>
*Display*:
![Display](img/display.png)
>
*Update*:
![Update](img/update.png)
>
*Delete*:
![Delete](img/delete.png)
>
*SQL*:
![SQL](img/deletesql.png)




